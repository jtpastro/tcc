\documentclass[../cic-tc.tex]{subfiles}
\begin{document}

\chapter{Gramática ILOC}
\label{iloc}

Um programa ILOC consiste em uma lista sequencial de operações.
Cada operação pode ser precedida por um rótulo.
Um rótulo é apenas uma cadeia de caracteres sendo separada da instrução por dois pontos.
Por convenção, limita-se o formato dos rótulos com a expressão regular \texttt{[a-z]([a-z]|[0-9]|-)*}.
Se alguma operação precisa de mais de um rótulo, deve ser inserido uma instrução que contém apenas um operação \texttt{nop} antes dela, colocando o rótulo adicional na instrução \texttt{nop}.
Um programa ILOC é definido mais formalmente:

\begin{tabular}{lll}

ProgramaILOC    &  $\rightarrow$  &  ListaOperacoes \\  
ListaOperacoes  &  $\rightarrow$  &  Operacao \\
                &  $\vert$ &  \texttt{label:} Operacao \\                
		&  $\vert$ & Operacao \texttt{;} ListaOperacoes \\
		&  $\vert$ & \texttt{label:} Operacao \texttt{;} ListaOperacoes \\
\end{tabular}

Uma operação ILOC corresponde a uma instrução em nível de máquina que
pode ser executada por uma única unidade funcional em um único
ciclo. Ela tem um código de operação (\emph{opcode}), uma sequência de
operandos fontes separados por vírgulas, e uma sequência de operandos
alvo separados também por vírgulas. Os operandos fonte são separados
dos operandos alvo pelo símbolo \ARROW, que significa
``em''. Formalmente:

\begin{tabular}{lll}
  Operacao    &  $\rightarrow$  &  OperacaoNormal \\
                       & $\vert$ & OperacaoFluxoControle \\
  OperacaoNormal & $\rightarrow$  &  CodigoOperacao ListaOperandos \texttt{=>} ListaOperandos \\
  ListaOperandos & $\rightarrow$  &  Operando \\
                       & $\vert$ & Operando \texttt{,} ListaOperandos \\
  Operando & $\rightarrow$  &  \texttt{registrador} \\
                       & $\vert$ & \texttt{numero} \\
                       & $\vert$ & \texttt{rotulo} \\
\end{tabular}

O não-terminal \emph{CodigoOperacao} pode ser qualquer operação ILOC,
exceto \texttt{cbr}, \texttt{jump}, e \texttt{jumpI}. As tabelas na
seção~\ref{iloc-tabelas} mostram o número de operandos e seus tipos
para cada operação da Linguagem ILOC.

Um \emph{Operando} pode ser um de três tipos: \texttt{registrador},
\texttt{numero} e \texttt{rotulo}. O tipo de cada operando é
determinado pelo código da operação e a posição que o operando aparece
na operação. Por convenção, os registradores começam pela letra
\texttt{r} (minúscula) e são seguidos por um número inteiro ou uma
cadeia de caracteres qualquer. Ainda por convenção, rótulos sempre
começam pela letra \texttt{L} (maiúscula).

A maioria das operações tem um único operando alvo; algumas operações
de armazenamento (\emph{store}) tem operandos alvos múltiplos, assim
como saltos. Por exemplo, \texttt{storeAI} tem um único operando fonte
e dois operandos alvo. A fonte deve ser um registrador, e os alvos
devem ser um registrador e uma constante imediata. Então, a operação
da linguagem ILOC:

\texttt{storeAI ri => rj,4} 

calcula o endereço adicionando \texttt{4} ao conteúdo de \texttt{rj} e
armazena o valor encontrado no registrador \texttt{ri} na localização da
memória especificada pelo endereço calculado. Em outras palavras:

\textsc{Memória}(\texttt{rj + 4}) $\leftarrow$ \textsc{Conteúdo}(\texttt{ri})

Operações de fluxo de controle tem uma sintática diferente. Uma vez
que estas operações não definem seus alvos, elas são escritas com uma
flecha simples \texttt{->} ao invés da flecha dupla
\texttt{=>}. Formalmente:

\begin{tabular}{lll}
  OperacaoFluxoControle    &  $\rightarrow$  &  \texttt{cbr register -> label, label}  \\
                       & $\vert$ & \texttt{jumpI -> label} \\
                       & $\vert$ & \texttt{jumpI -> register}
\end{tabular}

A primeira operação, \texttt{cbr}, implementa um desvio
condicional. As outras duas operações são desvios incondicionais.

\section{Convenções de Nome}
O código ILOC usa um conjunto simples de convenções de nome.
\begin{enumerate}
\item Deslocamentos de memória para variáveis são representados
  simbolicamente com um \texttt{@} antes do nome da variável.
\item Existe um número ilimitado de registradores. Estes são nomeados com inteiros simples, como \texttt{r1789}, ou com nomes simbólicos, como em \texttt{ri} ou \texttt{rj}.
\item O registrador \texttt{rarp} é reservado como um ponteiro para o registro de ativação atual. Sendo assim, a operação:
  \texttt{loadAI rarp,@x => r1}
  carrega o conteúdo da variável \texttt{x}, guardada no deslocamento
  \texttt{@x} a partir do \texttt{rarp}, em \texttt{r1}.
\end{enumerate}

Comentários em ILOC começam com \texttt{//} e continuam até o final da linha.

\section{Operações Individuais}
\subsection{Aritmética}
A Linguagem ILOC tem operações de três endereços de registrador para registrador.

\begin{tabular}{llll|l}\toprule
{\bf Opcode} & {\bf Fonte}  &        & {\bf Alvo} & {\bf Significado} \\\toprule
\et{add}     & \et{r1, r2}  & \ARROW & \et{r3}    & $r3 = r1 + r2$ \\
\et{sub}     & \et{r1, r2}  & \ARROW & \et{r3}    & $r3 = r1 - r2$ \\
\et{mult}    & \et{r1, r2}  & \ARROW & \et{r3}    & $r3 = r1 * r2$ \\
\et{div}     & \et{r1, r2}  & \ARROW & \et{r3}    & $r3 = r1 / r2$ \\\midrule
\et{addI}    & \et{r1, c2}  & \ARROW & \et{r3}    & $r3 = r1 + c2$ \\
\et{subI}    & \et{r1, c2}  & \ARROW & \et{r3}    & $r3 = r1 - c2$ \\
\et{rsubI}   & \et{r1, c2}  & \ARROW & \et{r3}    & $r3 = c2 - r1$ \\
\et{multI}   & \et{r1, c2}  & \ARROW & \et{r3}    & $r3 = r1 * c2$ \\
\et{divI}    & \et{r1, c2}  & \ARROW & \et{r3}    & $r3 = r1 / c2$ \\
\et{rdivI}   & \et{r1, c2}  & \ARROW & \et{r3}    & $r3 = c2 / r1$ \\\bottomrule
\end{tabular}

Todas estas operações realizam a leitura dos operandos origem de registradores ou constantes e escrevem o resultado de volta para um registrador.
Qualquer registrador pode servir como um operando origem ou destino.

As primeiras quatro operações da tabela são operações registrador para registrador clássicas.
As próximas seis especificam um operando imediato.
As operações não comutativas, \et{sub} e \et{div}, tem duas formas imediatas alternativas para permitir o operando imediato em qualquer lado do operador.
As formas imediatas são úteis para expressar resultados de certas otimizações, para escrever exemplos de forma mais concisa, e para registrar jeitos óbvios de reduzir a demanda por registradores.

\subsection{Shifts}
ILOC suporta um conjunto de operações aritméticas de \emph{shift}, para a esquerda e para a direita, em ambas as formas, com registradores e imediata.

\begin{tabular}{llll|l}\toprule
{\bf Opcode} & {\bf Fonte}  &        & {\bf Alvo} & {\bf Significado} \\\toprule
\et{lshift}  & \et{r1, r2}  & \ARROW & \et{r3}    & $r3 = r1 << r2$ \\
\et{lshiftI} & \et{r1, c2}  & \ARROW & \et{r3}    & $r3 = r1 << c2$ \\
\et{rshift}  & \et{r1, r2}  & \ARROW & \et{r3}    & $r3 = r1 >> r2$ \\
\et{rshiftI} & \et{r1, c2}  & \ARROW & \et{r3}    & $r3 = r1 >> c2$ \\\bottomrule
\end{tabular}

\subsection{Operações sobre a Memória}
ILOC suporta um conjunto de operadores de carga e armazenamento para mover valores entre a memória e registradores. As operações \et{load} e \et{cload} movem dados da memória para os registradores.

\begin{tabular}{llll|l}\toprule
{\bf Opcode} & {\bf Fonte}  &        & {\bf Alvo} & {\bf Significado} \\\toprule
\et{load}    & \et{r1}      & \ARROW & \et{r2}    & $r2 =$ \textsc{Memoria}($r1$) \\
\et{loadAI}  & \et{r1, c2}  & \ARROW & \et{r3}    & $r3 =$ \textsc{Memoria}($r1 + c2$) \\
\et{loadA0}  & \et{r1, r2}  & \ARROW & \et{r3}    & $r3 =$ \textsc{Memoria}($r1 + r2$) \\
\et{cload}   & \et{r1}     & \ARROW & \et{r2}     & caractere \et{load} \\
\et{cloadAI} & \et{r1, c2} & \ARROW & \et{r3}     & caractere \et{loadAI} \\
\et{cloadA0} & \et{r1, r2} & \ARROW & \et{r3}     & caractere \et{loadA0} \\\bottomrule
\end{tabular}

As operações diferem nos modos de endereçamento que elas suportam.
As operações \et{load} e \et{cload} assumem um endereço direto na forma de um único operando registrador.
As operações \et{loadAI} e \et{cloadAI} adicionam um valor imediato ao conteúdo do registrador para formar um endereço imediatamente antes de realizar a carga.
Nós chamamos estas de operações de \emph{endereçamento imediato}.
As operações \et{loadA0} e \et{cloadA0} adicionam o conteúdo de dois registradores para calcular o endereço efetivo imediatamente antes de realizar a carga.
Estas operações são chamadas de \emph{endereçamento por deslocamento}.

Uma outra forma de carga que a Linguagem ILOC suporta é uma operação \et{loadI} de carga imediata.
Ela recebe um inteiro como argumento e coloca este inteiro dentro do registrador alvo.

\begin{tabular}{llll|l}\toprule
{\bf Opcode} & {\bf Fonte}  &        & {\bf Alvo} & {\bf Significado} \\\toprule
\et{loadI}    & \et{c1}      & \ARROW & \et{r2}    & $r2 = c1$ \\\bottomrule
\end{tabular}

As operações de armazenamento são semelhantes, conforme a tabela abaixo.

\begin{tabular}{llll|l}\toprule
{\bf Opcode} & {\bf Fonte}  &        & {\bf Alvo}     & {\bf Significado} \\\toprule
\et{store}    & \et{r1}     & \ARROW & \et{r2}        & \textsc{Memoria}($r2$) $= r1$ \\
\et{storeAI}  & \et{r1}     & \ARROW & \et{r2, c3}    & \textsc{Memoria}($r2 + c3$) $= r1$ \\
\et{storeA0}  & \et{r1}     & \ARROW & \et{r2, r3}    & \textsc{Memoria}($r2 + r3$) $= r1$\\
\et{cstore}   & \et{r1}     & \ARROW & \et{r2}        & caractere \et{store} \\
\et{cstoreAI} & \et{r1}     & \ARROW & \et{r2, c3}    & caractere \et{storeAI} \\
\et{cstoreA0} & \et{r1}     & \ARROW & \et{r2, r3}    & caractere \et{storeA0} \\\bottomrule
\end{tabular}

Não há nenhuma operação de armazenamento imediato.

\subsection{Operações de Cópia entre Registradores}
A linguagem ILOC possui um conjunto de operações para mover valores entre registradores diretamente, sem acesso à memória.
As operações interpretam os valores como inteiro ou caractere codificado em ASCII e permitem converter entre os dois tipos.

\begin{tabular}{llll|l}\toprule
{\bf Opcode} & {\bf Fonte} &        & {\bf Alvo}     & {\bf Significado} \\\toprule
\et{i2i}     & \et{r1}     & \ARROW & \et{r2}        & $r2 = r1$ para inteiros \\
\et{c2c}     & \et{r1}     & \ARROW & \et{r2}        & $r2 = r1$ para caracteres \\
\et{c2i}     & \et{r1}     & \ARROW & \et{r2}        & converte um caractere para um inteiro\\
\et{i2c}     & \et{r1}     & \ARROW & \et{r2}        & converte um inteiro para caractere \\\bottomrule
\end{tabular}

As primeiras duas operações, \et{i2i} e \et{c2c}, copiam um valor de
um registrador para outro, sem conversão. As duas últimas operações
realizam conversões considerando a codificação de caracteres ASCII.

\subsection{Operações de Fluxo de Controle}
Em geral, operações de comparação na Linguagem ILOC recebem dois
valores e retornam um valor booleano.

\begin{tabular}{llll|l}\toprule
{\bf Opcode} & {\bf Fonte} &        & {\bf Alvo}     & {\bf Significado} \\\toprule
\et{cmp\_LT} & \et{r1, r2} & \ARROWs & \et{r3}        & $r3 = true$ se $r1 < r2$, senão $r3 = false$ \\
\et{cmp\_LE} & \et{r1, r2} & \ARROWs & \et{r3}        & $r3 = true$ se $r1 \leq r2$, senão $r3 = false$ \\
\et{cmp\_EQ} & \et{r1, r2} & \ARROWs & \et{r3}        & $r3 = true$ se $r1 = r2$, senão $r3 = false$ \\
\et{cmp\_GE} & \et{r1, r2} & \ARROWs & \et{r3}        & $r3 = true$ se $r1 \geq r2$, senão $r3 = false$ \\
\et{cmp\_GT} & \et{r1, r2} & \ARROWs & \et{r3}        & $r3 = true$ se $r1 > r2$, senão $r3 = false$ \\
\et{cmp\_NE} & \et{r1, r2} & \ARROWs & \et{r3}        & $r3 = true$ se $r1 \neq r2$, senão $r3 = false$ \\\midrule
\et{cbr}     & \et{r1}     & \ARROWs & \et{l2, l3}    & $PC = l2$ se $r1 = true$, senão $PC = l3$ \\\bottomrule
\end{tabular}

A operação condicional \et{cbr} recebe um booleano como argumento e
transfere o controle para um de dois rótulos alvo. Os dois rótulos
alvo não precisam estar definidos previamente (pode-se saltar para um
código mais a frente do programa).

\subsection{Saltos}
A Linguagem ILOC tem duas formas de operações de salto. A primeira é
um salto incondicional e imediato que transfere o controle para um a
primeira instrução após um rótulo. A segunda recebe um registrador
como argumento. O conteúdo do registrador é interpretado como um
endereço de código, transferindo o controle incondicionalmente e
imediatamente para este endereço. \emph{Esta segunda forma deve ser
  evitada por ser ambígua.} Mais detalhes a respeito disto na
referência oficial~\cite{keith}.

\begin{tabular}{llll|l}\toprule
{\bf Opcode} & {\bf Fonte} &        & {\bf Alvo}     & {\bf Significado} \\\toprule
\et{jumpI} &  & \ARROWs & \et{l1}        & $PC = l1$ \\
\et{jump}  &  & \ARROWs & \et{r1}        & $PC = r1$ \\\bottomrule
\end{tabular}

\section{Sumário de Operações ILOC}
\label{iloc-tabelas}


\begin{tabular}{llll|l}\toprule
{\bf Opcode} & {\bf Fonte}  &        & {\bf Alvo} & {\bf Significado} \\\toprule
\et{nop}     &&&                                  & não faz nada   \\\midrule

\et{add}     & \et{r1, r2}  & \ARROW & \et{r3}    & $r3 = r1 + r2$ \\
\et{sub}     & \et{r1, r2}  & \ARROW & \et{r3}    & $r3 = r1 - r2$ \\
\et{mult}    & \et{r1, r2}  & \ARROW & \et{r3}    & $r3 = r1 * r2$ \\
\et{div}     & \et{r1, r2}  & \ARROW & \et{r3}    & $r3 = r1 / r2$ \\\midrule

\et{addI}    & \et{r1, c2}  & \ARROW & \et{r3}    & $r3 = r1 + c2$ \\
\et{subI}    & \et{r1, c2}  & \ARROW & \et{r3}    & $r3 = r1 - c2$ \\
\et{rsubI}   & \et{r1, c2}  & \ARROW & \et{r3}    & $r3 = c2 - r1$ \\
\et{multI}   & \et{r1, c2}  & \ARROW & \et{r3}    & $r3 = r1 * c2$ \\
\et{divI}    & \et{r1, c2}  & \ARROW & \et{r3}    & $r3 = r1 / c2$ \\
\et{rdivI}   & \et{r1, c2}  & \ARROW & \et{r3}    & $r3 = c2 / r1$ \\\midrule

\et{lshift}  & \et{r1, r2}  & \ARROW & \et{r3}    & $r3 = r1 << r2$ \\
\et{lshiftI} & \et{r1, c2}  & \ARROW & \et{r3}    & $r3 = r1 << c2$ \\
\et{rshift}  & \et{r1, r2}  & \ARROW & \et{r3}    & $r3 = r1 >> r2$ \\
\et{rshiftI} & \et{r1, c2}  & \ARROW & \et{r3}    & $r3 = r1 >> c2$ \\\midrule

\et{and}     & \et{r1, r2}  & \ARROW & \et{r3}    & $r3 = r1 \land r2$ \\
\et{andI}    & \et{r1, c2}  & \ARROW & \et{r3}    & $r3 = r1 \land c2$ \\
\et{or}      & \et{r1, r2}  & \ARROW & \et{r3}    & $r3 = r1 \lor r2$ \\
\et{orI}     & \et{r1, c2}  & \ARROW & \et{r3}    & $r3 = r1 \lor c2$ \\
\et{xor}      & \et{r1, r2}  & \ARROW & \et{r3}    & $r3 = r1$ xor $r2$ \\
\et{xorI}     & \et{r1, c2}  & \ARROW & \et{r3}    & $r3 = r1$ xor $c2$ \\\midrule

\et{i2i}     & \et{r1}     & \ARROW & \et{r2}        & $r2 = r1$ para inteiros \\
\et{c2c}     & \et{r1}     & \ARROW & \et{r2}        & $r2 = r1$ para caracteres \\
\et{c2i}     & \et{r1}     & \ARROW & \et{r2}        & converte um caractere para um inteiro\\
\et{i2c}     & \et{r1}     & \ARROW & \et{r2}        & converte um inteiro para caractere \\\bottomrule
\end{tabular}

\begin{table}[tp]
\begin{tabular}{llll|l}\toprule
{\bf Opcode} & {\bf Fonte} &        & {\bf Alvo}     & {\bf Significado} \\\toprule
\et{loadI}    & \et{c1}      & \ARROW & \et{r2}    & $r2 = c1$ \\\bottomrule
\et{load}    & \et{r1}      & \ARROW & \et{r2}    & $r2 =$ \textsc{Memoria}($r1$) \\
\et{loadAI}  & \et{r1, c2}  & \ARROW & \et{r3}    & $r3 =$ \textsc{Memoria}($r1 + c2$) \\
\et{loadA0}  & \et{r1, r2}  & \ARROW & \et{r3}    & $r3 =$ \textsc{Memoria}($r1 + r2$) \\\midrule

\et{cload}   & \et{r1}     & \ARROW & \et{r2}     & caractere \et{load} \\
\et{cloadAI} & \et{r1, c2} & \ARROW & \et{r3}     & caractere \et{loadAI} \\
\et{cloadA0} & \et{r1, r2} & \ARROW & \et{r3}     & caractere \et{loadA0} \\\midrule

\et{store}    & \et{r1}     & \ARROW & \et{r2}        & \textsc{Memoria}($r2$) $= r1$ \\
\et{storeAI}  & \et{r1}     & \ARROW & \et{r2, c3}    & \textsc{Memoria}($r2 + c3$) $= r1$ \\
\et{storeA0}  & \et{r1}     & \ARROW & \et{r2, r3}    & \textsc{Memoria}($r2 + r3$) $= r1$\\\midrule

\et{cstore}   & \et{r1}     & \ARROW & \et{r2}        & caractere \et{store} \\
\et{cstoreAI} & \et{r1}     & \ARROW & \et{r2, c3}    & caractere \et{storeAI} \\
\et{cstoreA0} & \et{r1}     & \ARROW & \et{r2, r3}    & caractere \et{storeA0} \\\midrule
\et{jumpI} &  & \ARROWs & \et{l1}        & $PC = l1$ \\
\et{jump}  &  & \ARROWs & \et{r1}        & $PC = r1$ \\\midrule

\et{cbr}     & \et{r1}     & \ARROWs & \et{l2, l3}    & $PC = l2$ se $r1 = true$, senão $PC = l3$ \\\midrule

\et{cmp\_LT} & \et{r1, r2} & \ARROWs & \et{r3}        & $r3 = true$ se $r1 < r2$, senão $r3 = false$ \\
\et{cmp\_LE} & \et{r1, r2} & \ARROWs & \et{r3}        & $r3 = true$ se $r1 \leq r2$, senão $r3 = false$ \\
\et{cmp\_EQ} & \et{r1, r2} & \ARROWs & \et{r3}        & $r3 = true$ se $r1 = r2$, senão $r3 = false$ \\
\et{cmp\_GE} & \et{r1, r2} & \ARROWs & \et{r3}        & $r3 = true$ se $r1 \geq r2$, senão $r3 = false$ \\
\et{cmp\_GT} & \et{r1, r2} & \ARROWs & \et{r3}        & $r3 = true$ se $r1 > r2$, senão $r3 = false$ \\
\et{cmp\_NE} & \et{r1, r2} & \ARROWs & \et{r3}        & $r3 = true$ se $r1 \neq r2$, senão $r3 = false$ \\\bottomrule
\label{tab:sum_iloc}
\end{tabular}
\caption{Operações de fluxo de controle e sobre a memória}
\end{table}

\chapter{Código do Simulador}

\end{document}
